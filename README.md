#!/bin/bash
# TMW GM Log Downloader (original): https://gitlab.com/liviorecchia/tmw-gm-log-downloader
# TMW GM Log Downloader(deprecated): https://gitlab.com/Ledmitz/tmw-gm-log-downloader
# tmw-gmlog(deprecated): https://gitlab.com/Ledmitz/tmw-gmlog
# tmw-info: https://gitlab.com/Ledmitz/tmw-info
# The Mana World: https://themanaworld.org]
# Ledmitz (2020) - GPL 3.0


Requires: Linux
Suggests: zenity

Intro:
	Although much of this information is available on wiki and server pages, browsers can pose some issues; Load time, navigation and
browser fingerprinting are a few. The convenience and speed of having basic information available in the terminal can't be matched.
	tmw-info's update functions download database(DB) files to be searched locally and only require updating after a server update,
except in the case of GM Logs, where you may wish to update before searching, if looking for the newest issued commands.
	All searches ignore case. Only GM log searches can use any(search) and exact(esearch) terms. Other searches use all terms only.
	Because tmw-info searches the actual DB files, hidden attributes are seen in the results.

Tips:
	When searching for references of items and mobs, remember to check it's ID and search with it too. When items and mobs are spawned,
ID's are often used, rather than names. Item IDs for drops are in mob DB files, but names are not. It is important to remember that the
files being searched contain IDs and are in some cases, like drops, converted into human readable text for viewing results in the terminal.
When searching for monster drops, always use the item ID, which can be found easily in an item search. Item searches do not require an ID
as it's name is searchable in the DB. Also note that the name in game may differ in rare cases, like the Magic Ring being the MageRing.
	gm-show is the only GUI aspect of tmw-info at the moment, but defaults to terminal output if zenity isn't installed (I didn't
uninstall zenity to test, but echoing to terminal should work. Let me know if I'm wrong please). It works the exact same as tmw-gmlog and
is available separately on my projects page (see top). It is intented to be called from a preset keyboard shortcut (I run ManaPlus in
window mode and use OS KB shortcuts to toggle fullscreen. Even when in fullscreen, the GUI window will show on top this way when the key
combo is pressed). It is also important to mention, there is a delay between the GM CMDs issued and the server's log being updated.
	If trying to compile a complete DB file of all items, for instance, a common character must be found in all references because
wildcards will not work. " , " can be used to search all files since it is the common string separator in all, but will have a lot of
empty space and junk lines and even some misplaced information. Due to the IDs having different character lengths (no preceeding 0s),
sorting them in the proper order can also be a challenge. You can encounter orders like this: 5524, 56, 5576, 558.
If you still wish to create a full list: " grep -E '^[0-9]+,\s*' | sort -n | tee -a  items.txt < <(tmw-info item-search ,) "
Due to the subshell, you will have to manually quit when the output stops with ctrl-c before the file is populated. I'll work on making it
pipe properly in the future.

Note:
	Due to TMW becoming deprecated with the coming of the new rEvolt server, future versions of this script will be sparse or unlikely
unless requested, though I may make a wiki search for very general infomation. More work will be put into making a version of this script
to be used with rEvolt in the near future and will, hopefully, not need too much reworking. I would like to make "Other Attributes" a bit
more human readable though.

Help:
tmw-info [command] [STRING]
    help / --help / -h (this help screen)
    cmds-update (downloads command source files. Should be run before using "cmds")
    cmds (ManaPlus and GM command reference)
    news (reads game news)
    online (shows online players)
    gm-update (downloads all GM logs. Should be run before searching)
    gm-search TERM1 TERM2 TERM3 ... (results contain any of the terms)
    gm-asearch TERM1 TERM2 TERM3 ... (results contain all of the terms)
    gm-esearch TERM1 TERM2 TERM3 ... (results contain the exact term)
    gm-show (if zenity is installed, displays a GUI pop-up of the last 10 issued GM CMDs), otherwise in terminal
    chat-search TERM1 TERM2 TERM3 ... (chat: results contain all of the terms)
    item-update (downloads item DB. Must be run before searching)
    item-search TERM1 TERM2 TERM3 ... (items: results contain all of the terms)
    mob-update (downloads monster DB. Must be run before searching)
    mob-search TERM1 TERM2 TERM3 ... (monsters: results contain all of the terms)
If you get and error while searching, it may contain special characters and needs to be quoted


2020-08-15 - fixed gm-show (wasn't showing GUI) and removed trailing comma in mob drops
2020-08-16 - added ratio for dropped items to represent chances in a more human readable way
2020-09-16 - added chat log search
2021-04-13 - added cmds-update to download @ command DB pages and jak1's wiki on client commands
           - "cmds" now gets info from those pages and is better organized
           - added extra function to chat-search to view a log file easily after a search
           - fixed a bug that downloaded 2020-09 GM log everytime gm-update was run
           - added support for GM log files ending in "-old" (2020-09-old is a restored file)
